# Python Parser
Python Project that will parse a set of data and normalized it 
Email and Password.

The input set of data is stored in example_1.csv

The output is stored on the savefile.txt

## Installation

pip install pandas
pip install numpy

## Usage

```python


import numpy as np

np.savetxt("savefile.txt", np.array(normalize_data), fmt="%s")
```
