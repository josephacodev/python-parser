import re
import csv
import numpy as np

ip_regex = '''^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
            25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
            25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.( 
            25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)'''

email_regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
normalize_data = []
print(f'Processing...')
with open('example_1.csv') as fobj:
    for line in fobj:
        line_data = re.split(':|;|,| ', line)
        if(re.search(email_regex,line_data[0])):
            if len(line_data) > 2:
                if(re.search(ip_regex, line_data[1])):
                    stringData = line_data[0] + ' ' + line_data[2].rstrip()
                    normalize_data.append(stringData)
                else:
                    stringData = line_data[0] + ' ' + line_data[2].rstrip()
                    normalize_data.append(stringData)
            else:
                stringData = line_data[0] + ' ' + line_data[1].rstrip()
                normalize_data.append(stringData)
    np.savetxt("savefile.txt", np.array(normalize_data), fmt="%s")

    # np.savetxt("savefile.txt", np.array(normalize_data), fmt="%s") separate


           
         
